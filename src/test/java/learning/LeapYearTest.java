package learning;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests here! Get yer tests!
 * @author S. Hall on 3/2/2016.
 */
public class LeapYearTest {

    @Test
    public void testIsLeapYear_forMultiplesOfFourHundred_true() throws Exception {
        LeapYear leapYear = new LeapYear();
        assertEquals(true, leapYear.isLeapYear(2000));
    }

    @Test
    public void testIsLeapYear_forMultiplesOfOneHundred_false() throws Exception {
        LeapYear leapYear = new LeapYear();
        assertEquals(false, leapYear.isLeapYear(1900));
    }

    @Test
    public void isLeapYear_forNumbersNotDivisibleByFour_false(){
        LeapYear leapYear = new LeapYear();
        assertEquals(false, leapYear.isLeapYear(1999));
    }
}